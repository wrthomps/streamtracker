package view;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class STTrayIcon {
	
	private TrayIcon trayIcon;
	
	public void init(final Application app)
	{
		// Do nothing if there is no system tray support
		if (!SystemTray.isSupported()) return;
		
		// Initialize the system tray with the icon
		SystemTray tray = SystemTray.getSystemTray();
		Image image = Toolkit.getDefaultToolkit().getImage("icon.gif");
		
		// Create the popup menu
		PopupMenu popup = new PopupMenu();
		MenuItem restoreItem = new MenuItem("Restore");
		MenuItem exitItem = new MenuItem("Exit");
		
		// Treat the "Exit" option as closing the window
		ActionListener exitListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				app.new WindowExitAdapter().windowClosing(null);
				System.exit(0);
			}
		};
		
		// Treat the "Restore" option as a deiconification
		ActionListener restoreListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				app.new WindowRestoreAdapter().windowDeiconified(null);
			}
		};
		
		restoreItem.addActionListener(restoreListener);
		exitItem.addActionListener(exitListener);
		
		popup.add(restoreItem);
		popup.add(exitItem);
		
		trayIcon = new TrayIcon(image, "StreamTracker", popup);
		
		trayIcon.setImageAutoSize(true);
		
		trayIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				app.new WindowRestoreAdapter().windowDeiconified(null);
			}
		});
		
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("Could not be added");
		}
	}

	/** Displays the popup notification when a new stream comes online */
	public void displayPopupMessage(String streamOnline)
	{
		if (!SystemTray.isSupported()) return;
		
		trayIcon.displayMessage("New stream online!", streamOnline + " has come online!", TrayIcon.MessageType.INFO);
	}
}
