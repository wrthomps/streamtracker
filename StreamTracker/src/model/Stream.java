package model;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Stream implements Comparable<Stream> {

	private String channel;
	private boolean online;
	private int viewers;
	
	// Standard constructor
	public Stream(String channel)
	{
		this.channel = channel;
		online = false;
		viewers = 0;
	}
	
	// Getters and setters
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}
	public int getViewers() {
		return viewers;
	}
	public void setViewers(int viewers) {
		this.viewers = viewers;
	}
	
	// Two streams are equal if their channels are the same word, ignoring case
	public boolean equals(Object o)
	{
		if (!(o instanceof Stream)) return false;
		Stream s = (Stream)o;
		
		if (this.getChannel().equalsIgnoreCase(s.getChannel())) return true;
		return false;
	}
	
	// Refresh the stream information by making a call to the Justin API
	public void refresh()
	{		
		// Build the XML into a DOM tree for parsing
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		// Connect to the API and get the response
		try
		{
			// Build the DOM tree
			builder = builderFactory.newDocumentBuilder();
			Document document = builder.parse("http://api.justin.tv/api/stream/list.xml?channel="+channel);
			document.getDocumentElement().normalize();
			
			Element root = document.getDocumentElement();
			
			// If <streams> tag has no children, no online stream matched the request
			if (root.getChildNodes().getLength() == 0)
			{
				online = false;
				viewers = 0;
				return;
			}
			
			else
			{
				// <streams> tag has at most one child named <stream>
				NodeList list = document.getElementsByTagName("stream");
				Node item = list.item(0);
				
				// Get viewer count
				if (item.getNodeType() == Node.ELEMENT_NODE)
				{
					Element e = (Element) item;
					viewers = Integer.parseInt(getTagValue("channel_count", e));
				}
				online = true;
				return;
			}
		}
		catch (MalformedURLException e)
		{
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (SAXException e) {
			System.out.println(e.getMessage());
		} catch (ParserConfigurationException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private static String getTagValue(String sTag, Element eElement)
	{
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
		
        Node nValue = (Node) nlList.item(0);
        
        return nValue.getNodeValue();
    }

	@Override
	public int compareTo(Stream o) {
		
		return this.getChannel().compareToIgnoreCase(o.getChannel());
	}
	
}
