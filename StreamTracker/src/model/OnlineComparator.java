package model;

import java.util.Comparator;

/** Compares two streams, preferring online streams to offline streams.
 * Used to sort when the "group online streams" option is checked
 * 
 * @author Woody Thompson
 *
 */
public class OnlineComparator implements Comparator<Stream> {

	@Override
	public int compare(Stream s, Stream t) {
		
		// If the streams are both online or both offline, compare alphabetically
		if (!(s.isOnline() ^ t.isOnline()))
			return s.getChannel().compareToIgnoreCase(t.getChannel());
		
		// Favor online streams to offline streams
		else if (s.isOnline() && !t.isOnline()) return -1;
		else if (!s.isOnline() && t.isOnline()) return 1;
		
		return 0;
	}

}
