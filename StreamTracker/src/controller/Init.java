/**
 * 
 */
package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Woody Thompson
 *
 */
public class Init {

	
	/** Initializes the StreamTracker
	 * Uses the file config.ini to build the list of favorited streams. The list should consist of
	 * the channel names of favorited streams, one per line.
	 */
	public static String[] init(ArrayList<String> streams)
	{
		// Get the list of favorited streams
		File f = new File("config.ini");
		
		// Create the config file if it doesn't exist
		if (!f.exists())
		{
			try
			{
				f.createNewFile();
			}
			catch (IOException e)
			{
				System.out.print(e.getMessage());
				System.exit(1);
			}
		}
		
		// Parse the file and build the ArrayList
		try
		{
			Scanner s = new Scanner(f);
			
			while(s.hasNextLine())
				streams.add(s.nextLine());
		}
		catch (FileNotFoundException e)
		{
			System.out.print(e.getMessage());
			System.exit(1);
		}
		
		// Turn the ArrayList into a sorted array
		String[] streamsArray = streams.toArray(new String[0]);
		Arrays.sort(streamsArray, String.CASE_INSENSITIVE_ORDER);
		
		return streamsArray;
	}

}
