package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import model.Stream;

public class Uninit {
	
	/** Write out the array of streams to the config file for the next launch */
	public static void uninit(Stream[] streams, File f)
	{
		// Delete the file if it already exists
		if (f.exists())
			f.delete();
		
		// Remake the file
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		// Write out the streams back to the file
		try {
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			for (int i = 0; i < streams.length; i++)
			{
				bw.write(streams[i].getChannel());
				bw.newLine();
			}
			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
